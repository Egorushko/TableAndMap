//
//  EAMMasterTableViewController.m
//  ATConsulting
//
//  Created by user123369 on 9/12/17.
//  Copyright © 2017 user123369. All rights reserved.
//

#import "EAMMasterTableViewController.h"

@interface EAMMasterTableViewController ()

@property(assign, nonatomic) bool flagOutside;
@property(assign, nonatomic) NSInteger row;

@end

@implementation EAMMasterTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
       self.presenter = [[EAMDataPresenter alloc] initWithTableViewProtocol:self];
    self.flagOutside = false;
    self.row = 1000;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSInteger count = self.visitModel.count;
    return count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString* cellIdentifier = @"cell";
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if(!cell)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                      reuseIdentifier:cellIdentifier];
    }
    NSInteger index = indexPath.row;
    NSString* title = ((EAMVisitModel*)self.visitModel[indexPath.row]).titleVisit;
    NSString* organizationKey = ((EAMVisitModel*)self.visitModel[index]).organizationId;
    NSString* detailTitle = ((EAMOrganizationModel*)[self.organizationDictionary objectForKey:organizationKey]).title;
    cell.textLabel.text = title;
    cell.detailTextLabel.text = detailTitle;
    if ([organizationKey isEqualToString:self.selOrgId] && self.flagOutside) {
        cell.backgroundColor = [UIColor greenColor];
    }
    else if (index == self.row && !self.flagOutside)
    {
        cell.backgroundColor = [[UIColor greenColor] colorWithAlphaComponent:0.8];
    }
    else
    {
        if (index % 2 == 0)
        {
            cell.backgroundColor = [[UIColor yellowColor] colorWithAlphaComponent:0.7];
        }
        else
        {
            cell.backgroundColor = [[UIColor yellowColor] colorWithAlphaComponent:0.2];
        }
        

    }
    return cell;
}


#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView
didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString* orgId = ((EAMVisitModel*)self.visitModel[indexPath.row]).organizationId;
    [self.presenter changeSelectOrganizationId:orgId];
    self.flagOutside = false;
    self.row = indexPath.row;
    [self.tableView reloadData];
}

#pragma mark - EAMTableViewProtocol
-(void) updateData: (id) newData
{
    if([newData isKindOfClass:[NSArray class]])
    {
        NSArray* array = (NSArray*)newData;
        if ([array[0] isKindOfClass:[EAMVisitModel class]]) {
            self.visitModel = newData;
        }
    }
    else if ([newData isKindOfClass:[NSDictionary class]])
    {
        self.organizationDictionary = (NSDictionary*)newData;
    }
    
    
    if(self.organizationDictionary && self.visitModel)
    {
        [self.tableView reloadData];
    }
    
    
}
-(void) changeSelectOrganizationId: (NSString*) organizationId
{
    if (!self.presenter.isMyContrllerDidSelect) {
        self.flagOutside = true;
        self.row = 1000;
        self.selOrgId = organizationId;
        [self.tableView reloadData];

    }
    
}

@end
