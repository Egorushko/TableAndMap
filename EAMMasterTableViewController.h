//
//  EAMMasterTableViewController.h
//  ATConsulting
//
//  Created by user123369 on 9/12/17.
//  Copyright © 2017 user123369. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EAMTableViewProtocol.h"
#import "EAMVisitModel.h"
#import "EAMOrganizationModel.h"
#import "EAMDataPresenter.h"


@interface EAMMasterTableViewController : UITableViewController <UITableViewDelegate, UITableViewDataSource, EAMTableViewProtocol>

@property (strong, nonatomic) NSArray* visitModel;
@property (strong, nonatomic) NSDictionary* organizationDictionary;
@property (strong, nonatomic) EAMDataPresenter* presenter;
@property (strong, nonatomic) NSString* selOrgId;



#pragma mark - EAMTableViewProtocol

-(void) updateData: (id) newData;
-(void) changeSelectOrganizationId: (NSString*) organizationId;

@end
