//
//  EAMRequestData.h
//  ATConsulting
//
//  Created by user123369 on 9/13/17.
//  Copyright © 2017 user123369. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AFNetworking/AFNetworking.h>
typedef void (^JSONParser) (id obj);

@interface EAMRequestData : NSObject

- (instancetype)initWithURLForRequest: (NSString*) URLForRequest
                               parser: (JSONParser) parser;

- (void) getDataModel;

@end
