//
//  EAMDataInterface.h
//  ATConsulting
//
//  Created by user123369 on 9/13/17.
//  Copyright © 2017 user123369. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EAMRequestData.h"
#import "EAMVisitModel.h"
#import "EAMOrganizationModel.h"

extern NSString* const EAMVisitModelsChangeNotification;
extern NSString* const EAMVisitModelsUserInfoKey;
extern NSString* const EAMOrganizationModelsChangeNotification;
extern NSString* const EAMOrganizationModelsUserInfoKey;
extern NSString* const EAMOrganizationIdChangeNotification;
extern NSString* const EAMOrganizationIdUserInfoKey;



@interface EAMDataInterface : NSObject

-(void) getData;
-(void) changeSelectOrganizationId: (NSString*) organizationId;

@end
