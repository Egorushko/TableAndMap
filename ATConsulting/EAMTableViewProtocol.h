//
//  EAMTableViewProtocol.h
//  ATConsulting
//
//  Created by user123369 on 9/13/17.
//  Copyright © 2017 user123369. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol EAMTableViewProtocol <NSObject>

@required

-(void) updateData: (id) newData;
-(void) changeSelectOrganizationId: (NSString*) organizationId;

@end
