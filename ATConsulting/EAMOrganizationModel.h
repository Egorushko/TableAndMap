//
//  EAMOrganizationModel.h
//  ATConsulting
//
//  Created by user123369 on 9/13/17.
//  Copyright © 2017 user123369. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

@interface EAMOrganizationModel : NSObject

@property (assign,nonatomic) NSString* organizationId;
@property (strong, nonatomic) NSString* title;
@property (assign,nonatomic) CGFloat latitude;
@property (assign,nonatomic) CGFloat longitude;

- (instancetype)initWithOrganizationId:(NSString*) organizationId
                                 title:(NSString*) title
                              latitude:(CGFloat) latitude
                             longitude:(CGFloat)longitude;

@end
