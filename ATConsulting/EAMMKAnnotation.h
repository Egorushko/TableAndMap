//
//  EAMMKAnnotation.h
//  ATConsulting
//
//  Created by user123369 on 9/14/17.
//  Copyright © 2017 user123369. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface EAMMKAnnotation : NSObject <MKAnnotation>

@property (strong, nonatomic) NSString* orgId;

- (instancetype)initWithLatitude: (CGFloat) latitude
                       longitude: (CGFloat)longitude
                           title: (NSString*) title;

#pragma mark - MKAnnotation

@property (nonatomic) CLLocationCoordinate2D coordinate;
@property (nonatomic, copy) NSString *title;

@end
