//
//  EAMRequestData.m
//  ATConsulting
//
//  Created by user123369 on 9/13/17.
//  Copyright © 2017 user123369. All rights reserved.
//

#import "EAMRequestData.h"

@interface EAMRequestData ()
@property (strong, nonatomic) NSString* URLForRequest;
@property (strong, nonatomic) AFHTTPSessionManager* requestManager;
@property (copy, nonatomic) JSONParser parser;
@end

@implementation EAMRequestData

- (instancetype)initWithURLForRequest: (NSString*) URLForRequest
                               parser: (JSONParser) parser
{
    self = [super init];
    if (self) {
        self.URLForRequest = URLForRequest;
        self.requestManager = [[AFHTTPSessionManager alloc] initWithBaseURL:nil];
        self.parser = parser;
    }
    return self;
}

- (void) getDataModel
{
    
    __weak EAMRequestData* weakSelf = self;
    [self.requestManager GET:self.URLForRequest
                  parameters:nil
                    progress:nil
                     success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                         NSLog(@"%@", responseObject);
                         weakSelf.parser(responseObject);
                     } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                         NSLog(@"%@", [error localizedDescription]);
                     }];
    
}

@end
