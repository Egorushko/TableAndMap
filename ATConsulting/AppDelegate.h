//
//  AppDelegate.h
//  ATConsulting
//
//  Created by user123369 on 9/12/17.
//  Copyright © 2017 user123369. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EAMDataInterface.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic, readonly) EAMDataInterface* interface;



@end

