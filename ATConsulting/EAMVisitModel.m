//
//  EAMVisitsModel.m
//  ATConsulting
//
//  Created by user123369 on 9/13/17.
//  Copyright © 2017 user123369. All rights reserved.
//

#import "EAMVisitModel.h"

@implementation EAMVisitModel

- (instancetype)initWithTitleVisit: (NSString*) titleVisit
                    organizationId: (NSString*) organizationId

{
    self = [super init];
    if (self) {
        self.titleVisit = titleVisit;
        self.organizationId = organizationId;
    }
    return self;
}

@end
