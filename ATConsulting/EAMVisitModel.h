//
//  EAMVisitsModel.h
//  ATConsulting
//
//  Created by user123369 on 9/13/17.
//  Copyright © 2017 user123369. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface EAMVisitModel : NSObject

@property (strong, nonatomic) NSString* titleVisit;
@property (assign, nonatomic) NSString* organizationId;

- (instancetype)initWithTitleVisit: (NSString*) titleVisit
                    organizationId: (NSString*) organizationId;

@end
