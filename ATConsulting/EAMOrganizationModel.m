//
//  EAMOrganizationModel.m
//  ATConsulting
//
//  Created by user123369 on 9/13/17.
//  Copyright © 2017 user123369. All rights reserved.
//

#import "EAMOrganizationModel.h"

@implementation EAMOrganizationModel

- (instancetype)initWithOrganizationId:(NSString*) organizationId
                                 title:(NSString*) title
                              latitude:(CGFloat) latitude
                             longitude:(CGFloat)longitude

{
    self = [super init];
    if (self) {
        self.organizationId = organizationId;
        self.title = title;
        self.latitude = latitude;
        self.longitude = longitude;
        
    }
    return self;
}

@end
