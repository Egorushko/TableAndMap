//
//  EAMDataPresenter.h
//  ATConsulting
//
//  Created by user123369 on 9/13/17.
//  Copyright © 2017 user123369. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EAMTableViewProtocol.h"
#import "EAMDataInterface.h"
#import "AppDelegate.h"

@interface EAMDataPresenter : NSObject

@property (weak, nonatomic) id<EAMTableViewProtocol> tableViewProtocol;

@property (strong, nonatomic) NSArray* visitModels;
@property (strong, nonatomic) NSDictionary* organizationDictionary;
@property (assign, nonatomic) bool isMyContrllerDidSelect;


-(void) changeSelectOrganizationId: (NSString*) organizationId;
- (instancetype)initWithTableViewProtocol: (id <EAMTableViewProtocol>) obj;

@end
