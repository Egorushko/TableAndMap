//
//  EAMMKAnnotation.m
//  ATConsulting
//
//  Created by user123369 on 9/14/17.
//  Copyright © 2017 user123369. All rights reserved.
//

#import "EAMMKAnnotation.h"


@implementation EAMMKAnnotation 

- (instancetype)initWithLatitude: (CGFloat) latitude
                       longitude:(CGFloat)longitude
                           title: (NSString*) title
{
    self = [super init];
    if (self) {
        self.coordinate = CLLocationCoordinate2DMake(latitude, longitude);
        self.title = title;
        
    
    }
    return self;
}

@end
