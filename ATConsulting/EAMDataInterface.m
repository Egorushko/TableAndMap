//
//  EAMDataInterface.m
//  ATConsulting
//
//  Created by user123369 on 9/13/17.
//  Copyright © 2017 user123369. All rights reserved.
//

#import "EAMDataInterface.h"

NSString* const EAMURLRequestVisits = @"http://demo3526062.mockable.io/getVisitsListTest";
NSString* const EAMURLRequestOrganization = @"http://demo3526062.mockable.io/getOrganizationListTest";

NSString* const EAMVisitModelsChangeNotification = @"EAMVisitModelsChangeNotification";
NSString* const EAMVisitModelsUserInfoKey = @"EAMVisitModelsUserInfoKey";
NSString* const EAMOrganizationModelsChangeNotification = @"EAMOrganizationModelsChangeNotification";
NSString* const EAMOrganizationModelsUserInfoKey = @"EAMOrganizationModelsUserInfoKey";
NSString* const EAMOrganizationIdChangeNotification = @"EAMOrganizationIdChangeNotification";
NSString* const EAMOrganizationIdUserInfoKey = @"EAMOrganizationIdUserInfoKey";



@interface EAMDataInterface ()

@property (strong, nonatomic) EAMRequestData* requestVisits;
@property (strong, nonatomic) EAMRequestData* requestOrganization;
@property (strong, nonatomic, readwrite) NSArray* visitModels;
@property (strong, nonatomic, readwrite) NSArray* organizationModels;
@property (strong, nonatomic, readwrite) NSString* organizationId;

@end


@implementation EAMDataInterface



- (instancetype)init
{
    self = [super init];
    if (self) {
        __weak EAMDataInterface* weekSelf = self;
        self.requestVisits = [[EAMRequestData alloc]
        initWithURLForRequest:EAMURLRequestVisits
        parser:^(id obj) {
            NSMutableArray* mutArrayOfModels = [[NSMutableArray alloc] init];
            NSArray* responseArray = (NSArray*) obj;
            for (NSDictionary* dictionary in responseArray) {
                    NSString* title = [dictionary objectForKey:@"title"];
                    NSString* orgId = [dictionary objectForKey:@"organizationId"];
                    EAMVisitModel* visit = [[EAMVisitModel alloc] initWithTitleVisit:title organizationId:orgId];
                    [mutArrayOfModels addObject:visit];
            }
                               
            weekSelf.visitModels = mutArrayOfModels;
            }];
        
        self.requestOrganization = [[EAMRequestData alloc]
        initWithURLForRequest:EAMURLRequestOrganization
        parser:^(id obj) {
            NSMutableArray* mutArrayOfModels = [[NSMutableArray alloc] init];
            NSArray* responseArray = (NSArray*) obj;
            for (NSDictionary* dictionary in responseArray) {
                NSString* title = [dictionary objectForKey:@"title"];
                NSString* orgId = [dictionary objectForKey:@"organizationId"];
                CGFloat latitude = [[dictionary objectForKey:@"latitude"] doubleValue];
                CGFloat longitude = [[dictionary objectForKey:@"longitude"] doubleValue];
                EAMOrganizationModel* organization = [[EAMOrganizationModel alloc] initWithOrganizationId:orgId
                            title:title
                         latitude:latitude
                        longitude:longitude];
                [mutArrayOfModels addObject:organization];
                }
                                  
                weekSelf.organizationModels = mutArrayOfModels;
                }];
        self.organizationId = @"";
    }
    return self;
}


#pragma mark - Work With Data

-(void) changeSelectOrganizationId: (NSString*) organizationId
{
    self.organizationId = organizationId;
}

-(void) getData
{
    [self.requestVisits getDataModel];
    //NSLog(@"%@", self.visitModels);
    [self.requestOrganization getDataModel];
}

#pragma mark - Notifications

- (void) setVisitModels: (NSArray*) visitModels
{
    _visitModels = visitModels;
    NSDictionary* notificationDictionary = @{EAMVisitModelsUserInfoKey : visitModels};
    [[NSNotificationCenter defaultCenter]
     postNotificationName:EAMVisitModelsChangeNotification
                   object:nil
     userInfo:notificationDictionary];
}
- (void) setOrganizationModels: (NSArray*) organizationModels
{
    _organizationModels = organizationModels;
    NSDictionary* notificationDictionary = @{EAMOrganizationModelsUserInfoKey : organizationModels};
    [[NSNotificationCenter defaultCenter]
     postNotificationName:EAMOrganizationModelsChangeNotification
     object:nil
     userInfo:notificationDictionary];
}
- (void) setOrganizationId: (NSString*) organizationId
{
    _organizationId = organizationId;
    NSDictionary* notificationDictionary = @{EAMOrganizationIdUserInfoKey : organizationId};
    [[NSNotificationCenter defaultCenter]
     postNotificationName:EAMOrganizationIdChangeNotification
     object:nil
     userInfo:notificationDictionary];
}

@end
