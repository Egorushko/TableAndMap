//
//  EAMMapViewController.m
//  ATConsulting
//
//  Created by user123369 on 9/14/17.
//  Copyright © 2017 user123369. All rights reserved.
//

#import "EAMMapViewController.h"

@interface EAMMapViewController ()

-(void) zoomMap;
-(void) render;

@end

@implementation EAMMapViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.presenter = [[EAMDataPresenter alloc] initWithTableViewProtocol:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) zoomMap
{
    MKMapRect zoomRect = MKMapRectNull;
    
    for (id<MKAnnotation> annot in self.MapView.annotations) {
        CLLocationCoordinate2D location = annot.coordinate;
        MKMapPoint center = MKMapPointForCoordinate(location);
        static double delta = 20000;
        MKMapRect rect = MKMapRectMake(center.x-delta, center.y-delta, 2*delta, 2*delta);
        zoomRect= MKMapRectUnion(zoomRect, rect);
    }
    zoomRect = [self.MapView mapRectThatFits:zoomRect];
    [self.MapView setVisibleMapRect:zoomRect
                        edgePadding:UIEdgeInsetsMake(100, 100, 100, 100)
                           animated:true];
}

-(void) render
{
    NSMutableArray* mutArray = [[NSMutableArray alloc] init];
    for (EAMOrganizationModel* model in self.organizationModel)
    {
        EAMMKAnnotation* annot = [[EAMMKAnnotation alloc]
                                  initWithLatitude:model.latitude
                                  longitude:model.longitude
                                  title:model.title];
        annot.orgId = model.organizationId;
        
        [mutArray addObject:annot];
    }
    
    [self.MapView addAnnotations:mutArray];
    for (EAMMKAnnotation* annot in self.MapView.annotations) {
        if ([annot.orgId isEqualToString:self.selOrgId]) {
            [self.MapView selectAnnotation:annot animated:true];
        }
    }
    [self zoomMap];
}

#pragma  mark - MKMapViewDelegate
- (void)mapView:(MKMapView *)mapView
didSelectAnnotationView:(MKAnnotationView *)view
{
    
    NSString* orgId = ((EAMMKAnnotation*) view.annotation).orgId;
    ((MKPinAnnotationView*)view).pinTintColor = [UIColor blueColor];
    if(![orgId isEqualToString:self.selOrgId])
    {
        __weak EAMMapViewController* weakSelf = self;
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [weakSelf.presenter changeSelectOrganizationId:orgId];
            weakSelf.selOrgId=orgId;
        });
    }
    
    
}

- (void)mapView:(MKMapView *)mapView
didDeselectAnnotationView:(MKAnnotationView *)view
{
    ((MKPinAnnotationView*)view).pinTintColor = [UIColor redColor];
    [self.presenter changeSelectOrganizationId:@""];
}

- (nullable MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation
{
    NSString* orgId = ((EAMMKAnnotation*) annotation).orgId;
    static NSString* identifier = @"Annotation";
    MKPinAnnotationView* pin = (MKPinAnnotationView*)[mapView dequeueReusableAnnotationViewWithIdentifier:identifier];
    if(!pin)
    {
        pin = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:identifier];
        
        
        pin.canShowCallout = true;
    }
    else
    {
        pin.annotation = annotation;
    }
    
    
    if([self.selOrgId isEqualToString:orgId])
    {
        pin.pinTintColor = [UIColor blueColor];
        
    }
    else
    {
        pin.pinTintColor = [UIColor redColor];
    }
    return pin;
}



#pragma mark - EAMTableViewProtocol

-(void) updateData: (id) newData
{
    if([newData isKindOfClass:[NSArray class]])
    {
        NSArray* array = (NSArray*)newData;
        if ([array[0] isKindOfClass:[EAMOrganizationModel class]])
        {
            self.organizationModel = newData;
            
            [self render];
            
            
        }
    }
    
    
}
-(void) changeSelectOrganizationId: (NSString*) organizationId
{
    if(!self.presenter.isMyContrllerDidSelect)
    {
    self.selOrgId = organizationId;
    [self.MapView removeAnnotations:self.MapView.annotations];
    [self render];
    }

}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
