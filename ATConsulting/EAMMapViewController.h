//
//  EAMMapViewController.h
//  ATConsulting
//
//  Created by user123369 on 9/14/17.
//  Copyright © 2017 user123369. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "EAMOrganizationModel.h"
#import "EAMDataPresenter.h"
#import "EAMMKAnnotation.h"

@interface EAMMapViewController : UIViewController <MKMapViewDelegate, EAMTableViewProtocol>

@property (weak, nonatomic) IBOutlet MKMapView *MapView;
@property (strong, nonatomic) NSArray* organizationModel;
@property (strong, nonatomic) EAMDataPresenter* presenter;
@property (strong, nonatomic) NSString* selOrgId;



#pragma mark - EAMTableViewProtocol

-(void) updateData: (id) newData;
-(void) changeSelectOrganizationId: (NSString*) organizationId;

@end
