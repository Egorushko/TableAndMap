//
//  main.m
//  ATConsulting
//
//  Created by user123369 on 9/12/17.
//  Copyright © 2017 user123369. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
