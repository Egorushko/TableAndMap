//
//  EAMDataPresenter.m
//  ATConsulting
//
//  Created by user123369 on 9/13/17.
//  Copyright © 2017 user123369. All rights reserved.
//

#import "EAMDataPresenter.h"

@interface EAMDataPresenter ()



@property (strong, nonatomic, readwrite) NSArray* organizationModels;
@property (strong, nonatomic, readwrite) NSString* organizationId;
@property (strong, nonatomic) EAMDataInterface* dataInterface;

-(void) prepareDictianory;


-(void) dataChangeNotification: (id) data;
-(void) orgIdChangeNotification;

-(void) organizationChange: (NSNotification*) notification;
-(void) visitChange: (NSNotification*) notification;
-(void) orgIdChange: (NSNotification*) notification;

@end

@implementation EAMDataPresenter

- (instancetype)initWithTableViewProtocol: (id <EAMTableViewProtocol>) obj
{
    self = [super init];
    if (self) {
        self.isMyContrllerDidSelect = false;
        self.tableViewProtocol = obj;
        self.dataInterface = ((AppDelegate*)
                              [UIApplication sharedApplication].delegate).interface;
        [[NSNotificationCenter defaultCenter]
         addObserver:self
         selector:@selector(visitChange:)
         name:EAMVisitModelsChangeNotification
         object:nil];
        [[NSNotificationCenter defaultCenter]
         addObserver:self
         selector:@selector(organizationChange:)
         name:EAMOrganizationModelsChangeNotification
         object:nil];
        [[NSNotificationCenter defaultCenter]
         addObserver:self
         selector:@selector(orgIdChange:)
         name:EAMOrganizationIdChangeNotification
         object:nil];
        
    }
    return self;
}
- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - Functions

-(void) prepareDictianory
{
    NSMutableDictionary* dict = [[NSMutableDictionary alloc] init];
    for (EAMOrganizationModel* org in self.organizationModels) {
        [dict setObject:org forKey:org.organizationId];
    }
    self.organizationDictionary = dict;
}

-(void) changeSelectOrganizationId: (NSString*) organizationId
{
    self.isMyContrllerDidSelect = true;
    [self.dataInterface changeSelectOrganizationId:organizationId];
}


#pragma mark - Notifications
-(void) organizationChange: (NSNotification*) notification
{
    self.organizationModels = [notification.userInfo objectForKey:EAMOrganizationModelsUserInfoKey];
    [self dataChangeNotification:self.organizationModels];
    [self prepareDictianory];
    [self dataChangeNotification: self.organizationDictionary];
}

-(void) visitChange: (NSNotification*) notification
{
    self.visitModels = [notification.userInfo objectForKey:EAMVisitModelsUserInfoKey];
    
    [self dataChangeNotification: self.visitModels];
}
-(void) orgIdChange: (NSNotification*) notification
{
    
    self.organizationId = [notification.userInfo objectForKey:EAMOrganizationIdUserInfoKey];
    [self orgIdChangeNotification];
    
}
-(void) orgIdChangeNotification
{
    
    [self.tableViewProtocol changeSelectOrganizationId:self.organizationId];
    
    self.isMyContrllerDidSelect = false;
}



-(void) dataChangeNotification: (id) data
{
    [self.tableViewProtocol updateData:data];
    
}




@end
